# Lab. 12 - Clases

<img src="http://i.imgur.com/PA6wtGY.png?1" width="215" height="225">   <img src="http://i.imgur.com/Nps6FYs.png?1" width="215" height="225">   <img src="http://i.imgur.com/lbe9IUc.png?1" width="215" height="225">


Imágenes tomadas de [1], [2], [3].

La *programación orientada a objetos* (object oriented programming u OOP) es un paradigma de programación que promueve el diseño de programas en el que distintos objetos interactúan entre sí para resolver un problema.   C++ es uno de los lenguajes de programación que promueve la programación orientada a objetos, permitiendo que los programadores creen sus propias clases desde cero o derivadas de otras clases existentes. Algunos otros lenguajes que promueven OOP son Java, Python, javascript y PHP.  

En OOP, cada objeto encapsula dentro de él ciertas propiedades sobre el ente que está modelando (por ejemplo, un objeto que modela un *punto* encapsula dentro de sí las coordenadas *x* y *y* del punto que representa). Además, cada objeto permite realizar ciertas acciones sobre sí, i.e. contiene sus *métodos*. Por ejemplo, un objeto punto puede realizar la acción de cambiar el valor de su coordenada *x*. 

Cuando la clase de objetos que necesitamos utilizar en nuestro programa no ha sido predefinida en el alguna librería, necesitamos declarar e implementar nuestra propia clase. Para esto definimos *clases* que contengan datos con ciertas *propiedades* o *atributos* y acciones que queremos hacer con esos datos por medio de *métodos* o *funciones miembro*. De esta manera, podremos organizar la información y procesos en *objetos* que tienen las propiedades y métodos de una clase. 

&nbsp;

##Objetivos:

En esta experiencia de laboratorio los estudiantes practicarán el definir una clase e implementar algunos de sus métodos completando un programa que simula un partido de baloncesto entre dos jugadores, mantiene la puntuación del juego y las estadísticas globales de los dos jugadores.


&nbsp;

##Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos relacionados a clases y objetos.

2. haber estudiado el esqueleto del programa en `main.cpp` que puedes encontrar en esta página de Moodle; nota que gran parte del código está como comentarios, debes estudiar ese código también.

3. haber tomado el [quiz Pre-Lab 12](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6930) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


&nbsp;

##El juego:

El esqueleto de programa que incluimos simula un partido de baloncesto entre dos jugadores. Todos los tiros anotados reciben dos puntos. Se inicializan los datos con el nombre de cada jugador y sus estadísticas globales en el "torneo": total de juegos jugados, tiros intentados, tiros anotados y rebotes. El programa incluye funciones  aleatorias y fórmulas para determinar el jugador que intenta el tiro, si anota el tiro y el jugador que coge el rebote.  Durante el partido simulado se mantiene la puntuación de cada jugador y se actualizan datos para cada jugador. Al final del partido se despliega una tabla con las estadísticas.

&nbsp;

##Sesión de laboratorio:

Tu tarea durante la sesión de laboratorio será el definir la clase `BBPlayer` con los atributos y prototipos de los métodos que se listan abajo. El esqueleto del programa incluye el código para definir algunos de los métodos; otros tendrán que ser definidos.

El esqueleto de programa también incluye la función `test_BBPlayer` que hace pruebas unitarias para cada uno de las funciones del programa. Las pruebas están comentadas y, según vayas definiendo las funciones, las vas descomentando hasta que la prueba para esa función se pase. Una vez todas las funciones estén listas y hayan pasado las pruebas, se prueba el programa completo removiendo los comentarios en el código de la función `main`.

### Instrucciones:

1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab12-Classes.git` para descargar la carpeta `Lab12-Classes` a tu computadora.

2.  Marca doble "click" en el archivo `Basket01.pro` para cargar este proyecto a Qt. 

3. Este programa correrá en el terminal y  debemos seleccionar esa opción desde la ventana de "Projects". Para acceder a esta ventana, seleccionas "Projects" en el menú vertical de la izquierda. Luego en `Build & Run` seleccionas `Run` y luego marcas la caja que dice `Run in terminal`.

4. Define la clase `BBPlayer` con las especificaciones que se incluyen abajo. Para cada uno de los métodos que se especifican abajo:

    a. Incluye el propotipo del método en la definición de la clase

    b. Si la función correspondiente a ese método ya está codificada en el programa, remueve los comentarios de las secciones correspondientes a esa función en  `test_BBPlayer`.

    c. Si la función correspondiente a ese método no está codificada, define la función y luego remueve los comentarios de las secciones correspondientes a esa función en  `test_BBPlayer`.

    d. Corre el programa y verifica que haya pasado las pruebas. Debes obtener una ventana similar a la de la Figura 1. Si no pasa las pruebas, revisa tu código. Repite hasta que tu código haya pasado las pruebas. 

    <img src="http://i.imgur.com/pFmTkVr.png">

    **Figura 1.** Ejemplo de la pantalla que debes obtener si el código pasa las pruebas unitarias.


5. Una vez tengas todas las funciones definidas y probadas, descomenta el código de la función `main` para probar el programa completo. Si el programa funciona correctamente, debes obtener una ventana que comienza de manera similar a la Figura 2 y termina como en la Figura 3.

<img src="http://i.imgur.com/2Ep0WmQ.png">

**Figura 2.** Ejemplo del comienzo de la pantalla que debes obtener si el programa funciona correctamente.

<img src="http://i.imgur.com/yXyAMBm.png">

**Figura 3.** Ejemplo del final de la pantalla que debes obtener si el programa funciona correctamente.

**IMPORTANTE:** NO debes hacer ningún cambio en las funciones  `main` y `test_BBPlayer`, aparte de quitar los comentarios.


### Clase `BBPlayer`:

Define una clase `BBPlayer` que contenga los atributos y métodos que se describen abajo. Como algunas de las funciones que ya están definidas utilizan atributos y métodos de esta clase, es importante que utilices los mismos nombres que indicamos. El código comentado en la función `main` y en `test_BBPlayer` también te ayudará a determinar el tipo de dato que debe devolver el método y los tipos que deben tener los parámetros y el orden en que se incluyen en la declaración de la función.

**Atributos**

*  `_name`: guardará el nombre del jugador
*  `_shotsTaken`: guardará el número de tiros al canasto intentados durante el torneo
*  `_shotsMade`: guardará el número de tiros al canasto anotados durante el torneo
*  `_gamesPlayed`: guardará el número de juegos jugados durante el torneo
*  `_rebounds`: guardará el número de rebotes durante el torneo
*  `_score`: guardará la puntuación del jugador durante el partido

**Métodos**

* constructor por defecto (default). *(método incluido en `main.cpp`)*
* `setAll()`: método que asignará valor inicial a todos los atributos de un objeto. Nota que este método está invocado al comienzo de `main` para asignarle valores iniciales al arreglo `P` que es un arreglo de dos objetos de la clase `BBPlayer`.
* `name()`: método para adquirir el nombre del jugador.
* `shotPercentage()`: método para calcular el porciento de tiros anotados; se usa para "determinar" si el tiro intentado por un jugador se anota. *(método incluido en `main.cpp`)*
* `reboundsPerGame()`: método para calcular el promedio de rebotes cogidos por juego; se usa para "determinar" si un jugador coge un rebote. *(método incluido en `main.cpp`)*
* `shotMade()`: método que registra que se anotó un tiro, que se intentó un tiro y actualiza la puntuación del jugador.
* `shotMissed()`: método que registra tiro intentado (pero fallado).
* `reboundMade()`: método que registra un rebote cogido.
* `addGame()`: método que registra que se jugó un partido.
* `score()`: método para adquirir la puntuación del jugador.
* `printStats()`: método para desplegar las estádisticas del jugador. *(método incluido en `main.cpp`)*


&nbsp;

###Entrega.

Sube el archivo `main.cpp` con todo el código de tu programa utilizando Moodle [Entrega  del Lab 12](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=6931).




&nbsp;


## Referencias

[1] http://www.telegraph.co.uk/sport/olympics/basketball/9348826/London-2012-Olympics-Temi-Fagbenle-in-Team-GB-womens-basketball-squad.html

[2] http://www.musthavemenus.com/category/bar-clipart.html

[3] http://basketball.isport.com/basketball-guides/finding-your-niche-in-basketball


